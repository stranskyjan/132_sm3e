\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SM3Eprolem}[2020/17/02 Lecture notes for problem sessions of course 132SME3E]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{SessionNumber}
\newcounter{ProblemNumber}

\renewcommand{\maketitle}[2]{%
   \setcounter{SessionNumber}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Structural Mechanics 3 - 132SM3E | Problem session \Roman{SessionNumber}: #2}%
    \rfoot{\thepage}%
}

\newcommand{\ShortTest}[2]{%
    \paragraph{Five-minute problem}%
    \cite[#1]{Hibbeler:2019:SA}
    Determine the diagrams of non-zero internal forces. 
%
\begin{center}
 \includegraphics[width=#2\textwidth]{short_test-\theSessionNumber}%
\end{center}
%
\vspace*{-1em}\hrulefill\vspace*{-1em}
}

%\newcommand{\Navod}[1]{%
%    \paragraph{Návod}%
%    #1
%}

\newcommand{\SelfStudy}[1]{%
    \paragraph{Self-study resources}%
    #1
}

\newcommand{\Problem}[2]{%
    \refstepcounter{ProblemNumber}%
    \paragraph{Problem \Roman{SessionNumber}.\arabic{ProblemNumber}}%
    #1%
    \medskip\noindent%
    \emph{Hint.}~#2
}

%\newcommand{\PrikladVolitelny}[2]{%
%    \refstepcounter{ProblemNumber}%
%    \paragraph{Příklad \Roman{SessionNumber}.\arabic{ProblemNumber}$^\ast$}%
%    #1%
%    \medskip\noindent%
%    \emph{Kontrola.}~#2
%}

\newcommand{\References}{%
\bibliography{liter}
}

\newcommand{\Request}{%
\paragraph{Request} Plese feel free to submit any errors or recommendations using the issue tracker system at \url{https://gitlab.com/open-mechanics/teaching/132_sm3e/-/issues}\texttt{>New issue} (requires registration) or this \href{incoming+open-mechanics-teaching-132-sm3e-31018627-issue-@incoming.gitlab.com}{email address}.
}

%\newcommand{\HomeworkAssigment}[1]{%
%\bigskip%
%#1
%}