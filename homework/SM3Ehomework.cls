\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SM3Ehomework}[2022/23/02 Homework assigments for course 132SM3E]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{SessionNumber}

\renewcommand{\maketitle}[1]{%
   \setcounter{SessionNumber}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Structural Mechanics 3 - 132SM3E | Homework assigment No.~\arabic{SessionNumber}}%
    \rfoot{\thepage}%
}

\newcommand{\Problem}[2]{%
    \paragraph{Problem}%
    #1%
    \par\medskip\noindent%
    Image source:~\cite[p.~#2]{Hibbeler:2019:SA}
}

\newcommand{\References}{%
\bibliography{liter}
}

\newcommand{\Request}{%
\paragraph{Request} Plese feel free to submit any errors or recommendations using the issue tracker system at \url{https://gitlab.com/open-mechanics/teaching/132_sm3e/-/issues}\texttt{>New issue} (requires registration) or this \href{incoming+open-mechanics-teaching-132-sm3e-31018627-issue-@incoming.gitlab.com}{email address}.
}